<?php

namespace Delivery\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  IlluminateHttpRequest  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role) //aqui adiciona um parametro para o middleware.. usar nas routes ex: 'middleware' => 'auth.checkrole:client'
    {

        if(!Auth::check()) {
            return redirect('/auth/login');
        }

        if(Auth::user()->role <> $role) { //se a role do usuário autenticado bate com a $role que passamos
            return redirect('/auth/login');
        }

        return $next($request);
    }
}
