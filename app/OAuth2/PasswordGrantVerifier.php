<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 05/01/2018
 * Time: 14:38
 */

namespace Delivery\OAuth2;


use Illuminate\Support\Facades\Auth;

class PasswordGrantVerifier
{
    public function verify($username, $password)
    {
        $credentials = [
            'email'    => $username,
            'password' => $password,
        ];

        if (Auth::once($credentials)) {
            return Auth::user()->id;
        }

        return false;
    }
}