<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 01/01/2018
 * Time: 15:36
 */

namespace Delivery\Services;


use Delivery\Repositories\ClientRepository;
use Delivery\Repositories\UserRepository;

class ClientService
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(ClientRepository $clientRepository, UserRepository $userRepository)
    {

        $this->clientRepository = $clientRepository;
        $this->userRepository = $userRepository;
    }

    public function update(array $data, $id)
    {
        $this->clientRepository->update($data, $id);

        $userId = $this->clientRepository->find($id, ['user_id'])->user_id;

        $this->userRepository->update($data['user'], $userId);
    }

    public function create(array $data)
    {
        //criar senha padrao novo usuario
        $data['user']['password'] = bcrypt(123456);
        //cria usuario
        $user = $this->userRepository->create($data['user']);
        //vincula o id do usuario com o client
        $data['user_id'] = $user->id;
        //cria o client
        $this->clientRepository->create($data);
    }
}