<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 03/01/2018
 * Time: 21:53
 */

namespace Delivery\Services;


use Delivery\Models\Order;
use Delivery\Repositories\CupomRepository;
use Delivery\Repositories\ProductRepository;
use Delivery\Repositories\OrderRepository;

class OrderService
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var CupomRepository
     */
    private $cupomRepository;

    public function __construct(OrderRepository $orderRepository, ProductRepository $productRepository, CupomRepository $cupomRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
        $this->cupomRepository = $cupomRepository;
    }

    public function create(array $data)
    {
        \DB::beginTransaction();
        try{
            $data['status'] = 0;

            if(isset($data['cupom_id'])){
                unset($data['cupom_id']);
            }

            if (isset($data['cupom_code'])) {
                $cupom = $this->cupomRepository->findByField('code', $data['cupom_code'])->first();
                $data['cupom_id'] = $cupom->id;
                $cupom->used = 1;
                $cupom->save();
                unset($data['cupom_code']);
            }

            $items = $data['items'];
            unset($data['items']);

            $order = $this->orderRepository->create($data);

            $total = 0;

            foreach ($items as $item) {
                $item['price'] = $this->productRepository->find($item['product_id'])->price;
                $order->items()->create($item);
                $total += $item['price'] * $item['qtd'];
            }

            $order->total = $total;
            if(isset($cupom)){
                $order->total = $total - $cupom->value;
            }

            $order->save();
            \DB::commit();
            return $order;

        } catch (\Exception $e){
            \DB::rollback();
            throw $e;
        }
    }

    public function updateStatus($id, $idDeliveryman, $status){
        $order = $this->orderRepository->getByIdAndDeliveryman($id, $idDeliveryman);
        if($order instanceof Order){
            $order->status = $status;
            $order->save();
            return $order;
        }
        return false;
    }
}