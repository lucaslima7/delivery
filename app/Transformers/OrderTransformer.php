<?php

namespace Delivery\Transformers;

use League\Fractal\TransformerAbstract;
use Delivery\Models\Order;

/**
 * Class OrderTransformer
 * @package namespace Delivery\Transformers;
 */
class OrderTransformer extends TransformerAbstract
{
    //protected $defaultIncludes= ['cupom', 'items'];  //usando ou não ele 'manda' todos os arquivos


    protected $availableIncludes= ['cupom', 'items'];
    // $availableIncludes é o mais indicado pq é sob demanda, ou seja, so requisita o necessario
    // isso faz com que a aplicação se torne mais rápida.

    /**
     * Transform the Order entity
     * @param Delivery\Models\Order $model
     *
     * @return array
     */
    public function transform(Order $model)
    {
        return [
            'id'         => (int) $model->id,
            'total'      =>  (float) $model->total,
            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }

    public function includeClient(Order $model){
        return $this->item($model->client, new ClientTransformer());
    }

    public function includeCupom(Order $model){
        if(!$model->cupom){
            return null;
        }
        return $this->item($model->cupom,new CupomTransformer());
    }

    public function includeItems(Order $model){
        return $this->collection($model->items,new OrderItemTransformer());
    }
}
