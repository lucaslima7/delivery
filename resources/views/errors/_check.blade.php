<!--para aparecer msg de validação do formulário-->
@if($errors->any())
    <ul class="alert">
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif